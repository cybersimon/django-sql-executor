# README for django-sql-executor #

django-sql-executor is a very basic admin add-on for Django that allows admins to execute "SELECT" queries in raw SQL against the database, and download a CSV of the results.

### Installation ###

*  Add 'sqlexecutor' to INSTALLED_APPS.

* Add the following to urls.py (BEFORE the other admin URLs are included): 

```
#!python

(r'^admin/sqlexecutor/', include('sqlexecutor.admin_urls')),
```
* Syncdb 
```
#!shell

./manage.py syncdb
```