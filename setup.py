import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-sql-executor',
    version='0.0.1',
    packages=find_packages(),
    license='MIT License',
    description='A very basic admin add-on for Django that allows admins '
                'to execute "SELECT" queries in raw SQL against the database, '
                'and download a CSV of the results.',
    long_description=README,
    url='https://bitbucket.org/cybersimon/django-sql-executor',
    author='Simon Johnson',
    dependency_links = [],
    install_requires=[],
    classifiers = [
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    zip_safe=False,
)