from django.conf.urls import *

from sqlexecutor.admin_views import execute, export

urlpatterns = [
    url(r'^query/(?P<query_pk>\d+)/execute/$', execute, name='execute'),
    url(r'^query/(?P<query_pk>\d+)/export/$', export, name='export'),
]