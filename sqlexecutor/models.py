from django.db import models
from django.urls import reverse
from django.template import Template, Context

class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Query(models.Model):
    name = models.CharField(max_length=255)
    sql = models.TextField()
    description = models.TextField(blank=True)
    category = models.ForeignKey(Category, blank=True, null=True, on_delete=models.SET_NULL)
    
    def __unicode__(self):
        return self.name
    
    def full_name(self):
        if self.category:
            return u'%s - %s' % (self.category, self.name)
        else:
            return u'(Uncategorized) - %s' % self.name
    
    def execute(self):
        from django.db import connection
        cursor = connection.cursor()
        
        if self.sql.split()[0].lower() != 'select':
            return {'success': False, 'error_message': 'You can only execute SELECT queries.'}
        if self.sql.find(";") >= 0:
            return {'success': False, 'error_message': 'Sorry, you can only run one query at once. No ";" character allowed.'}

        # if there are some parameters, treat the query like a template.
        if self.parameter_set.all().count() > 0:
            sql_template = Template(self.sql)
            # build the context
            context = Context(
                {param.name: param.value for param in self.parameter_set.all()}
            )
            self.sql = sql_template.render(context)

        try:
            cursor.execute(self.sql)
            rows = cursor.fetchall()
            field_names = [field[0] for field in cursor.description]
            return {'success': True, 'field_names': field_names, 'rows': rows, 'num_rows': len(rows)}
        except Exception as e:
            return {'success': False, 'error_message': e}
    
    def execute_link(self):
        return '<a href="%s">Execute</a>' % reverse('execute', args=[self.pk])
    execute_link.allow_tags = True
    
    class Meta:
        verbose_name = 'Query'
        verbose_name_plural = 'Queries'    


class Parameter(models.Model):
    query = models.ForeignKey(Query, blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255)
    value = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    def __unicode__(self):
        return "%s: %s" % (self.name, self.value)
