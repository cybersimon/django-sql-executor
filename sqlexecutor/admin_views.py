import csv

from django.contrib.admin.views.decorators import staff_member_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.views.decorators.cache import never_cache
from django.http import HttpResponse

from .models import Query



def execute(request, query_pk):
    query = get_object_or_404(Query, pk=query_pk)
    
    context = query.execute()
    context.update({ 'query': query,  })
    
    return render_to_response('admin/sqlexecutor/query/execute.html',
                              context,
                              context_instance=RequestContext(request))
execute = staff_member_required(never_cache(execute))



def encode_list(row):
    encoded_row = []
    for item in row:
        if isinstance(item, unicode):
            encoded_row.append(item.encode('windows-1252', 'replace')) # Hack, so that it *mostly* works for Windows (some characters not encodable will be replaced by a question mark)
        else:
            encoded_row.append(item)
    return encoded_row

def export(request, query_pk):
    query = get_object_or_404(Query, pk=query_pk)
    
    result = query.execute()
    
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv; charset=windows-1252')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % query.name

    # Create the CSV writer using the HttpResponse as the "file"
    writer = csv.writer(response)
    writer.writerow(encode_list(result['field_names']))
    for row in result['rows']:
        encoded_row = encode_list(row)
        writer.writerow(encoded_row)

    return response
export = staff_member_required(never_cache(export))