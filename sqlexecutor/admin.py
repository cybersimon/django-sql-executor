from django.contrib import admin

from .models import Category, Query, Parameter


class ParameterInline(admin.TabularInline):
    model = Parameter
    extra = 1


class QueryAdmin(admin.ModelAdmin):
    list_display = ['full_name', 'execute_link', 'category']
    inlines = [ParameterInline, ]
    

admin.site.register(Category)
admin.site.register(Query, QueryAdmin)